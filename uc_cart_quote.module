<?php
/**
 * @file
 * Module file for uc_cart_quote.
 */


/**
 * Implements hook_menu().
 *
 */
function uc_cart_quote_menu() {
  $items = array();

  $items['cart/quote'] = array(
    'title' => 'Quote your cart',
    'description' => 'Quote your current cart contents.',
    'page callback' => 'uc_cart_quote_page',
    'access arguments' => array('access content'),
    'type' => MENU_CALLBACK,
  );

  $items['cart/quote/complete'] = array(
    'title' => 'Quote completed',
    'description' => 'Displays information upon completion of a quote.',
    'page callback' => 'uc_cart_quote_complete',
    'access arguments' => array('access content'),
    'type' => MENU_CALLBACK,
  );

  return $items;
}


/**
 * Implements hook_theme().
 */
function uc_cart_quote_theme($existing, $type, $theme, $path) {
  $theme_hooks = array(
    'uc_order__quote' => array(
      'template' => 'uc-order--quote',
      'path' => $path,
      'variables' => array()
    )
  );

  return $theme_hooks;
}

/**
 * Implements hook_theme
 */
function uc_cart_quote_theme_registry_alter(&$theme_registry) {
  $theme_registry['uc_order__quote']['variables'] = $theme_registry['uc_order']['variables'];
}


/**
 * Displays a form to send the quote. It gets composed of checkout panes enabled for this quote form.
 * Borrows extensevely from uc_cart_checkout.
 *
 * @see uc_cart_quote_form_alter
 * @see uc_cart_quote_filter_checkout_panes
 */
function uc_cart_quote_page() {
  global $user;

  $items = uc_cart_get_contents();
  if (count($items) == 0) {
    drupal_goto('cart');
  }

  // Send anonymous users to login page when anonymous checkout is disabled.
  if (!$user->uid && !variable_get('uc_cart_anonymous', TRUE)) {
    drupal_set_message(t('You must login before you can proceed to quote your cart.'));
    if (variable_get('user_register', 1) != 0) {
      drupal_set_message(t('If you do not have an account yet, you should <a href="!url">register now</a>.', array('!url' => url('user/register', array('query' => drupal_get_destination())))));
    }
    drupal_goto('user', array('query' => drupal_get_destination()));
  }

  // Load an order from the session, if available.
  if (isset($_SESSION['cart_order'])) {
    $order = uc_order_load($_SESSION['cart_order']);
    if ($order) {
      // Don't use an existing order if it has changed status or owner, or if
      // there has been no activity for 10 minutes (to prevent identity theft).
      if (uc_order_status_data($order->order_status, 'state') != 'in_checkout' ||
          ($user->uid > 0 && $user->uid != $order->uid) ||
          $order->modified < REQUEST_TIME - 600) {
        if (uc_order_status_data($order->order_status, 'state') == 'in_checkout' && $order->modified < REQUEST_TIME - 600) {
          // Mark expired orders as abandoned.
          uc_order_update_status($order->order_id, 'abandoned');
        }
        unset($order);
      }
    }
    else {
      // Ghost session.
      unset($_SESSION['cart_order']);
      drupal_set_message(t('Your session has expired or is no longer valid.  Please review your order and try again.'));
      drupal_goto('cart');
    }
  }

  // Determine whether the form is being submitted or built for the first time.

  if (isset($_POST['form_id']) && $_POST['form_id'] == 'uc_cart_quote_send_form') {
    // If this is a form submission, make sure the cart order is still valid.
    if (!isset($order)) {
      drupal_set_message(t('Your session has expired or is no longer valid.  Please review your quote and try again.'));
      drupal_goto('cart');
    }
    elseif (!empty($_SESSION['uc_cart_order_rebuild'])) {
      drupal_set_message(t('Your shopping cart contents have changed. Please review your quote and try again.'));
      drupal_goto('cart');
    }
  }
  else {
    // Prepare the cart order.
    $rebuild = FALSE;
    if (!isset($order)) {
      // Create a new order if necessary.
      $order = uc_order_new($user->uid);
      $_SESSION['quoting_order'] = $_SESSION['cart_order'] = $order->order_id;
      $order->data['is_quote'] = TRUE;
      $rebuild = TRUE;
    }
    elseif (!empty($_SESSION['uc_cart_order_rebuild'])) {
      // Or, if the cart has changed, then remove old products and line items.
      db_delete('uc_order_products')
        ->condition('order_id', $order->order_id)
        ->execute();
      uc_order_delete_line_item($order->order_id, TRUE);
      $rebuild = TRUE;
    }

    if ($rebuild) {
      // Copy the cart contents to the cart order.
      $order->products = uc_cart_get_contents();
      unset($_SESSION['uc_cart_order_rebuild']);
    }
    elseif (!uc_order_product_revive($order->products)) {
      drupal_set_message(t('Some of the products in this order are no longer available.'), 'error');
      drupal_goto('cart');
    }
  }

  return drupal_get_form('uc_cart_quote_send_form', $order);
}

/**
 * The quote form built up from the enabled checkout panes enabled for quotes.
 * It mirrors uc_cart_checkout_form functionality just adding an extra filter to panels.
 *
 * @param $order
 *   The order that is being checked out.
 *
 * @see uc_cart_quote_filter_checkout_panes()
 * @see uc_cart_checkout_form_validate()
 * @see uc_cart_checkout_form_review()
 * @see uc_cart_checkout_review()
 * @see theme_uc_cart_checkout_form()
 * @ingroup forms
 */
function uc_cart_quote_send_form($form, &$form_state, $order) {

  $panes = uc_cart_quote_filter_checkout_panes(_uc_checkout_pane_list());
  // If the order isn't shippable, remove panes with shippable == TRUE.
  if (!uc_order_is_shippable($order) && variable_get('uc_cart_delivery_not_shippable', TRUE)) {
    $panes = uc_cart_filter_checkout_panes($panes, array('shippable' => TRUE));
  }
  if (!count($panes)) {
    drupal_set_message(t('Apologies. There is an error with the quote feature. Please contact us to address this issue urgently.'), 'error');
    watchdog('uc_cart_quote', 'There are not checkout panes enabled for quotes. Feature has become unavailable.', array(), WATCHDOG_ERROR);
    drupal_goto('cart');
  }

  if ($processed = isset($form_state['storage']['order'])) {
    $order = $form_state['storage']['order'];
  }
  else {
    $form_state['storage']['order'] = $order;
    $form_state['storage']['base_path'] = implode('/', array_slice(arg(), 0, -1));
  }

  $form['#attributes']['class'][] = 'uc-cart-checkout-form';
  $form['#attributes']['class'][] = 'uc-cart-checkout-quote-form';
  $form['#attached']['js'][] = drupal_get_path('module', 'uc_cart') . '/uc_cart.js';
  $form['#attached']['css'][] = drupal_get_path('module', 'uc_cart') . '/uc_cart.css';

  if ($instructions = variable_get('uc_quote_instructions', '')) {
    $form['instructions'] = array(
      '#prefix' => '<div id="checkout-instructions" class="quote-instructions">',
      '#markup' => filter_xss_admin($instructions),
      '#suffix' => '</div>',
    );
  }

  $form['panes'] = array('#tree' => TRUE);

  // Invoke the 'prepare' op of enabled panes, but only if their 'process' ops
  // have not been invoked on this request (i.e. when rebuilding after AJAX).
  foreach ($panes as $id => $pane) {
    if ($pane['enabled'] && empty($form_state['storage']['panes'][$id]['prepared']) && isset($pane['callback']) && function_exists($pane['callback'])) {
      $pane['callback']('prepare', $order, $form, $form_state);
      $form_state['storage']['panes'][$id]['prepared'] = TRUE;
      $processed = FALSE; // Make sure we save the updated order.
    }
  }

  // Load the line items and save the order. We do this after the 'prepare'
  // callbacks of enabled panes have been invoked, because these may have
  // altered the order.
  if (!$processed) {
    $order->line_items = uc_order_load_line_items($order);
    uc_order_save($order);
  }

  foreach ($panes as $id => $pane) {
    if ($pane['enabled']) {
      $pane['prev'] = _uc_cart_checkout_prev_pane($panes, $id);
      $pane['next'] = _uc_cart_checkout_next_pane($panes, $id);

      if (!isset($pane['collapsed'])) {
        $collapsed = ($pane['prev'] === FALSE || empty($displayed[$pane['prev']])) ? FALSE : TRUE;
      }
      if (isset($form_state['expanded_panes']) && in_array($id, $form_state['expanded_panes'])) {
        $collapsed = FALSE;
      }

      $return = $pane['callback']('view', $order, $form, $form_state);

      // Add the pane if any display data is returned from the callback.
      if (is_array($return) && (!empty($return['description']) || !empty($return['contents']))) {
        // Create the fieldset for the pane.
        $form['panes'][$id] = array(
          '#type' => 'fieldset',
          '#title' => check_plain($pane['title']),
          '#description' => !empty($return['description']) ? $return['description'] : '',
          '#collapsible' => $pane['collapsible'] && variable_get('uc_use_next_buttons', FALSE),
          '#collapsed' => variable_get('uc_use_next_buttons', FALSE) ? $collapsed : FALSE,
          '#id' => $id . '-pane',
          '#theme' => isset($return['theme']) ? $return['theme'] : NULL,
        );

        // Add the contents of the fieldset if any were returned.
        if (!empty($return['contents'])) {
          $form['panes'][$id] = array_merge($form['panes'][$id], $return['contents']);
        }

        // Add the 'Next' button if necessary.
        if ((!isset($return['next-button']) || $return['next-button'] !== FALSE) && $pane['next'] !== FALSE &&
            variable_get('uc_use_next_buttons', FALSE) != FALSE) {
          $opt = variable_get('uc_collapse_current_pane', FALSE) ? $id : 'false';
          $form['panes'][$id]['next'] = array(
            '#type' => 'button',
            '#value' => t('Next'),
            '#weight' => 20,
            '#attributes' => array('onclick' => "return uc_cart_next_button_click(this, '" . $pane['next'] . "', '" . $opt . "');"),
            '#prefix' => '<div class="next-button">',
            '#suffix' => '</div>',
          );
        }

        // Log that this pane was actually displayed.
        $displayed[$id] = TRUE;
      }
    }
  }
  unset($form_state['expanded_panes']);

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['cancel'] = array(
    '#type' => 'submit',
    '#value' => t('Cancel'),
    '#validate' => array(), // Disable validation to prevent a new order
                            // from being created.
    '#limit_validation_errors' => array(),
    '#submit' => array('uc_cart_checkout_form_cancel'),
  );
  $form['actions']['continue'] = array(
    '#type' => 'submit',
    '#value' => t('Submit quote'),
  );

  unset($_SESSION['uc_checkout'][$order->order_id]);

  return $form;
}

/**
 * Form validation for uc_cart_quote_send_form().
 *
 * @see uc_cart_checkout_form()
 * @see uc_cart_checkout_form_submit()
 */
function uc_cart_quote_send_form_validate($form, &$form_state) {
  $order = $form_state['storage']['order'];

  // Update the order "modified" time to prevent timeout on ajax requests.
  $order->modified = REQUEST_TIME;

  // Validate/process the cart panes.  A FALSE value results in failed checkout.
  $form_state['checkout_valid'] = TRUE;
  foreach (element_children($form_state['values']['panes']) as $pane_id) {
    $func = _uc_checkout_pane_data($pane_id, 'callback');
    if (is_string($func) && function_exists($func)) {
      $isvalid = $func('process', $order, $form, $form_state);
      if ($isvalid === FALSE) {
        $form_state['expanded_panes'][] = $pane_id;
        $form_state['checkout_valid'] = FALSE;
      }
    }
  }

  // Reload line items and save order.
  $order->line_items = uc_order_load_line_items($order);
  uc_order_save($order);
}

/**
 * Form submission handler for uc_cart_checkout_form().
 *
 * @see uc_cart_checkout_form()
 * @see uc_cart_checkout_form_validate()
 */
function uc_cart_quote_send_form_submit($form, &$form_state) {
  $order = $form_state['storage']['order'];

  if ($form_state['checkout_valid'] === FALSE) {
    $url = $form_state['storage']['base_path'] . '/quote';
  }
  else {
    $url = $form_state['storage']['base_path'] . '/quote/complete';
    $_SESSION['uc_checkout'][$order->order_id]['do_quote'] = TRUE;
  }

  unset($form_state['checkout_valid']);

  $form_state['redirect'] = $url;
}

/**
 * Submit handler for "Cancel" button on uc_cart_checkout_form().
 *
 * @see uc_cart_checkout_form()
 */
function uc_cart_quote_send_form_cancel($form, &$form_state) {
  $order = $form_state['storage']['order'];
  if (isset($_SESSION['cart_order']) && $_SESSION['cart_order'] == $order->order_id) {
    uc_order_comment_save($_SESSION['cart_order'], 0, t('Customer canceled this quote from the quote form.'));
    unset($_SESSION['cart_order']);
    unset($_SESSION['quoting_order']);
  }

  unset($_SESSION['uc_checkout'][$order->order_id]);
  $form_state['redirect'] = $form_state['storage']['base_path'];
}


/**
 * Completes the sale and finishes checkout.
 */
function uc_cart_quote_complete() {
  if (empty($_SESSION['cart_order']) || empty($_SESSION['uc_checkout'][$_SESSION['cart_order']]['do_quote'])) {
    drupal_goto('cart');
  }

  $order = uc_order_load(intval($_SESSION['cart_order']));

  if (empty($order)) {
    // Display messages to customers and the administrator if the order was lost.
    drupal_set_message(t("We're sorry.  An error occurred while processing your quote that prevents us from completing it at this time. Please contact us and we will resolve the issue as soon as possible."), 'error');
    watchdog('uc_cart_quote', 'An empty order made it to checkout! Cart order ID: @cart_order', array('@cart_order' => $_SESSION['cart_order']), WATCHDOG_ERROR);
    drupal_goto('cart');
  }

  $build = uc_cart_quote_complete_quote($order, variable_get('uc_new_customer_login', FALSE));
  unset($_SESSION['uc_checkout'][$order->order_id], $_SESSION['cart_order']);

  // Add a comment to let sales team know this came in through the site.
  uc_order_comment_save($order->order_id, 0, t('Quote created through website.'), 'admin');

  $page = variable_get('uc_cart_quote_complete_page', '');
  if (!empty($page)) {
    drupal_goto($page);
  }

  return $build;
}


/**
 * Completes a sale, including adjusting order status and creating user account.
 *
 * @param $order
 *   The order object that has just been completed.
 * @param $login
 *   Whether or not to login a new user when this function is called.
 *
 * @return
 *   The HTML text of the default order completion page.
 */
function uc_cart_quote_complete_quote($order, $login = FALSE) {
  global $user;

  // Ensure we have the latest order data.
  $order->data = unserialize(db_query("SELECT data FROM {uc_orders} WHERE order_id = :order_id", array(':order_id' => $order->order_id))->fetchField());

  // Ensure that user creation and triggers are only run once.
  if (empty($order->data['complete_sale'])) {
    uc_cart_complete_sale_account($order);

    // Store account data.
    unset($order->data['is_quote']);
    db_update('uc_orders')
      ->fields(array(
        'uid' => $order->uid,
        'data' => serialize($order->data),
      ))
      ->condition('order_id', $order->order_id)
      ->execute();

    // Move an order's status from "In checkout" to "Quote".
    $status = db_query("SELECT order_status FROM {uc_orders} WHERE order_id = :order_id", array(':order_id' => $order->order_id))->fetchField();
    if (uc_order_status_data($status, 'state') == 'in_checkout') {
      $status = 'quote';
      if (uc_order_update_status($order->order_id, $status)) {
        $order->order_status = $status;
      }
    }

    // Invoke the checkout complete trigger and hook.
    $account = user_load($order->uid);
    module_invoke_all('u_quote_complete', $order, $account);
    rules_invoke_event('uc_quote_complete', $order);
  }

  $type = $order->data['complete_sale'];

  // Log in new users, if requested.
  if ($type == 'new_user' && $login && !$user->uid) {
    $type = 'new_user_logged_in';
    $user = user_load($order->uid);
  }

  $variables['!new_username'] = isset($order->data['new_user']['name']) ? $order->data['new_user']['name'] : '';
  $variables['!new_password'] = isset($order->password) ? $order->password : t('Your password');
  $messages = array(
    'uc_msg_quote_submit' => uc_get_message('quote_completion_message'),
    'uc_msg_quote_' . $type => uc_get_message('quote_completion_' . $type),
    'uc_msg_continue_shopping' => uc_get_message('continue_shopping'),
  );
  foreach ($messages as $id => &$message) {
    $message = variable_get($id, $message);
    $message = token_replace($message, array('uc_order' => $order));
    if ($id == 'uc_msg_quote_' . $type) {
      $message = strtr($message, $variables);
    }
  }
  $output = filter_xss_admin(implode(' ', $messages));

  // Empty that cart...
  uc_cart_empty(uc_cart_get_id(FALSE));

  return array(
    '#type' => 'module',
    '#theme' => 'uc_cart_complete_sale',
    '#message' => $output,
    '#order' => $order,
  );
}


/**
 * Implements hook_form_alter
 *
 * Attach a button to uc_cart_view_form from
 */
function uc_cart_quote_form_alter(&$form, &$form_state, $form_id) {
  if ($form_id == 'uc_cart_view_form') {
    $form['actions']['checkout']['quote'] = array(
      '#type' => 'submit',
      '#name' => 'quote-cart',
      '#value' => t('Quote cart'),
      '#submit' => array('uc_cart_view_form_submit', 'uc_cart_quote_prepare'),
      '#weight' => 10,
    );
  }
}

/**
 * Implements hook_default_rules_configuration().
 */
function uc_cart_quote_default_rules_configuration() {
  // Setup a default configuration for customer quote notifications.
  $rule = rules_reaction_rule();
  $rule->label = t('E-mail customer quote notification');
  $rule->active = TRUE;
  $rule->event('uc_quote_complete')
    ->action('uc_order_email_invoice', array(
      'order:select' => 'order',
      'from' => uc_store_email_from(),
      'addresses' => '[order:email]',
      'subject' => t('Your Quote at [store:name]'),
      'template' => 'quote',
      'view' => 'checkout-mail',
    ));

  $configs['uc_quote_customer_notification'] = $rule;

  // Setup a default predicate for admin checkout notifications.
  $rule = rules_reaction_rule();
  $rule ->label = t('E-mail admin quote notification');
  $rule->active = TRUE;
  $rule->event('uc_quote_complete')
    ->action('uc_order_email_invoice', array(
      'order:select' => 'order',
      'from' => uc_store_email_from(),
      'addresses' => uc_store_email(),
      'subject' => t('New Quote at [store:name]'),
      'template' => 'admin',
      'view' => 'admin-mail',
    ));

  $configs['uc_quote_admin_notification'] = $rule;

  return $configs;
}


/**
 * Implements hook_rules_event_info().
 */
function uc_cart_quote_rules_event_info() {
  $events['uc_quote_complete'] = array(
    'label' => t('Customer completes quote'),
    'group' => t('Cart'),
    'variables' => array(
      'order' => array(
        'type' => 'uc_order',
        'label' => t('Order'),
      ),
    ),
  );

  return $events;
}


/**
 * Implements hook_uc_checkout_pane_alter
 *
 * Add a quote_enabled key to default panels enabled for quote form.
 */
function uc_cart_quote_uc_checkout_pane_alter(&$panes) {
  $defaults = array('cart', 'customer', 'delivery', 'billing', 'comments');
  foreach ($defaults as $id) {
    if (isset($panes[$id])) $panes[$id]['quote_enabled'] = TRUE;
  }

  if (isset($_SESSION['quoting_order']) && isset($_SESSION['cart_order']) && $_SESSION['quoting_order'] == $_SESSION['cart_order'] && isset($panes['comments'])) {
    $panes['comments']['title'] = t('Quote comments');
  }
}


/**
 * Implements hook_uc_message().
 */
function uc_cart_quote_uc_message() {
  global $user;
  $messages['quote_completion_message'] = t('Your quote is complete! Your quote number is [uc_order:order-number].');
  $messages['quote_completion_logged_in'] = t('Thank you for your interest in shopping at [store:name]. While logged in, you may continue shopping or <a href="[uc_order:url]">view your current quote status</a> and order history.');
  $messages['quote_completion_existing_user'] = t("Thank you for your interest in shopping at [store:name]. Your current quote has been attached to the account we found matching your e-mail address.\n\n<a href=\"!user_url\">Login</a> to view your current quote status and order history. Remember to login when you make your next quote or purchase for a faster checkout experience!", array('!user_url' => url('user')));
  $messages['quote_completion_new_user'] = t("Thank you for your interest in shopping at [store:name]. A new account has been created for you here that you may use to view your current order status.\n\n<a href=\"!user_url\">Login</a> to your new account using the following information:\n\n<strong>Username:</strong> !new_username\n<strong>Password:</strong> !new_password", array('!user_url' => url('user')));
  $messages['quote_completion_new_user_logged_in'] = t("Thank you for your intetest in shopping at [store:name]. A new account has been created for you here that you may use to view your current quote status.\n\nYour password and further instructions have been sent to your e-mail address.\n\nFor your convenience, you are already logged in with your newly created account.");

  return $messages;
}


/**
 * Set form redirect to quote page
 */
function uc_cart_quote_prepare($form, &$form_state) {
  if (!uc_cart_get_id()) {
    return $form;
  }
  $form_state['redirect'] = 'cart/quote';
}


/**
 * Removes panes disabled for quote form from the pane list
 *
 * @return
 *   A checkout pane array with panes filtered out without
 *   a quote_enabled key set to true
 */
function uc_cart_quote_filter_checkout_panes($panes) {
  foreach ($panes as $id => $pane) {
    if (!isset($panes[$id]['quote_enabled']) || $panes[$id]['quote_enabled'] == FALSE)
      unset($panes[$id]);
  }

  return $panes;
}
